#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "KeyValueDecoder.hpp"
#include "doctest.h"

const std::string validInput1 = "39=D|44=15.1|38=1000";
const std::map<std::string, std::string> validResultOfInput1{
    {"39", "D"}, {"44", "15.1"}, {"38", "1000"}};


TEST_CASE("Expects to parse validInput1 and return correct value.") {
  KeyValueDecoder<'|', '='> decoder;
  std::map<std::string, std::string> result = decoder.parse(validInput1);
  
  REQUIRE_EQ(validResultOfInput1, result);
}

const std::string validInput2 = "39:D,44:15.1,38:1000";
const std::map<std::string, std::string> validResultOfInput2{
    {"39", "D"}, {"44", "15.1"}, {"38", "1000"}};


TEST_CASE("Expects to parse validInput2 and return correct value.") {
  KeyValueDecoder<',', ':'> decoder;
  std::map<std::string, std::string> result = decoder.parse(validInput2);
  
  REQUIRE_EQ(validResultOfInput2, result);
}

const std::string invalidInput = "39=D|44=15.1,38:1000";

TEST_CASE("Expects to throw after handing out bad input."){
  KeyValueDecoder<'|', ':'> decoder;
  CHECK_THROWS_AS(decoder.parse(invalidInput), std::invalid_argument);
}
