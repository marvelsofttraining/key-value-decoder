#pragma once
#include <map>
#include <sstream>
#include <string>

template <char delimiter, char assignment>
class KeyValueDecoder {
  public:
  KeyValueDecoder() = default;
  std::map<std::string, std::string> parse(const std::string& input) {
    std::map<std::string, std::string> toReturn;
    std::stringstream ss(input);
    std::string temp;

    while (std::getline(ss, temp, delimiter)) {
      //std::cout << "temp: " << temp << std::endl;
      toReturn.insert(getPair(temp));
    }

    return toReturn;
  }

  private:
  std::pair<std::string, std::string> getPair(const std::string& input) {
    auto delimiterPos = input.find_first_of(assignment);
    if (delimiterPos == std::string::npos) {
      throw std::invalid_argument("Bad input");
    }
    return std::make_pair<std::string, std::string>(
        input.substr(0, delimiterPos),
        input.substr(delimiterPos + 1, input.size()));
  }
};
